/**
 */
package org.eclipse.emf.teneo.samples.issues.refreshextra.util;

import org.eclipse.emf.common.util.URI;

import org.eclipse.emf.ecore.xmi.impl.XMLResourceImpl;

/**
 * <!-- begin-user-doc -->
 * The <b>Resource </b> associated with the package.
 * <!-- end-user-doc -->
 * @see org.eclipse.emf.teneo.samples.issues.refreshextra.util.RefreshextraResourceFactoryImpl
 * @generated
 */
public class RefreshextraResourceImpl extends XMLResourceImpl {
	/**
	 * Creates an instance of the resource.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param uri the URI of the new resource.
	 * @generated
	 */
	public RefreshextraResourceImpl(URI uri) {
		super(uri);
	}

} //RefreshextraResourceImpl
