/**
 * <copyright>
 *
 * Copyright (c) 2022 Springsite BV (The Netherlands) and others
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *   Martin Taal
 * </copyright>
 */

package org.eclipse.emf.teneo.hibernate.test.emf.demo;

import java.util.List;
import java.util.Properties;

import org.eclipse.emf.teneo.PersistenceOptions;
import org.eclipse.emf.teneo.samples.emf.sample.library.Book;
import org.eclipse.emf.teneo.samples.emf.sample.library.BookCategory;
import org.eclipse.emf.teneo.samples.emf.sample.library.Library;
import org.eclipse.emf.teneo.samples.emf.sample.library.LibraryFactory;
import org.eclipse.emf.teneo.samples.emf.sample.library.LibraryPackage;
import org.eclipse.emf.teneo.samples.emf.sample.library.Writer;
import org.eclipse.emf.teneo.test.AbstractTestAction;
import org.eclipse.emf.teneo.test.stores.TestStore;

/**
 * @author <a href="mailto:mtaal@elver.org">Martin Taal</a>
 * @version $Revision: 1.1 $
 */
public class LibraryDemoAction extends AbstractTestAction {

	public LibraryDemoAction() {
		super(LibraryPackage.eINSTANCE);
	}

	@Override
	public Properties getExtraConfigurationProperties() {
		final Properties props = new Properties();
		props.setProperty(PersistenceOptions.SET_PROXY, "true");
		props.setProperty(PersistenceOptions.JOIN_TABLE_FOR_NON_CONTAINED_ASSOCIATIONS, "true");
		props.setProperty(PersistenceOptions.ENABLE_AUDITING, "false");
		return props;
	}

	/** Creates an item, an address and links them to a po. */
	@Override
	public void doAction(TestStore store) {
		final LibraryFactory factory = LibraryFactory.eINSTANCE;
		// create a book, writer and library
		{
			store.beginTransaction();

			final Writer writer = factory.createWriter();
			writer.setName("JRR Tolkien");

			final Book book = factory.createBook();

			book.setAuthor(writer);
			book.setPages(5);
			book.setTitle("The Hobbit");
			book.setCategory(BookCategory.SCIENCE_FICTION_LITERAL);

			final Book book2 = factory.createBook();
			book2.setAuthor(writer);
			book2.setPages(5);
			book2.setTitle("The fellowship of the ring");
			book2.setCategory(BookCategory.SCIENCE_FICTION_LITERAL);

			final Library library = factory.createLibrary();
			library.getBooks().add(book);
			library.setName("Science Fiction Library");
			library.getBooks().add(book2);
			library.getWriters().add(writer);
			store.store(library);

			store.commitTransaction();
			assertEquals(2, writer.getBooks().size());
		}

		{
			store.beginTransaction();
			Library lib = (Library) store.query(Library.class, "name", "Science Fiction Library", 1)
					.get(0);
			List<Writer> writers = lib.getWriters();
			System.err.println(writers.size());
			List<Book> books = lib.getBooks();
			System.err.println(books.size());
			store.commitTransaction();
		}

		// throws exception
		if (false) {
			store.beginTransaction();
			Library lib = (Library) store.query(Library.class, "name", "Science Fiction Library", 1)
					.get(0);
			store.commitTransaction();
			List<Book> books = lib.getBooks();
			System.err.println(books.size());
		}

		{
			store.beginTransaction();

			final Writer writ = store.getObjects(Writer.class).get(0);
			final Library lib = (Library) writ.eContainer();

			final Book book = factory.createBook();
			book.setAuthor(writ);
			book.setPages(5);
			book.setTitle("test");
			book.setCategory(BookCategory.SCIENCE_FICTION_LITERAL);
			lib.getBooks().add(book);
			store.commitTransaction();
		}
	}
}
