/**
 * <copyright> Copyright (c) 2022 Springsite BV (The Netherlands) and others All rights
 * reserved. This program and the accompanying materials are made available under the terms of the
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html Contributors: Martin Taal </copyright> $Id:
 * DynamicAction.java,v 1.4 2007/03/20 23:33:38 mtaal Exp $
 */

package org.eclipse.emf.teneo.hibernate.test.emf.demo;

import java.util.Collection;
import java.util.List;
import java.util.Properties;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EEnumLiteral;
import org.eclipse.emf.ecore.EFactory;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.EcoreFactory;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.teneo.PersistenceOptions;
import org.eclipse.emf.teneo.hibernate.HbEntityDataStore;
import org.eclipse.emf.teneo.hibernate.test.stores.HibernateTestStore;
import org.eclipse.emf.teneo.hibernate.test.stores.adapters.HibernateTestDBAdapter;
import org.eclipse.emf.teneo.test.AbstractTestAction;
import org.eclipse.emf.teneo.test.stores.TestStore;
import org.eclipse.xsd.ecore.XSDEcoreBuilder;

/**
 * @author <a href="mailto:mtaal@elver.org">Martin Taal</a>
 * @version $Revision: 1.17 $
 */
public class DynamicDemoAction extends AbstractTestAction {

	private static final EPackage readEPackage(String xsdFile) {
		final String xsdLocation = "/home/mtaal/mydata/dev/git/"
				+ "org.eclipse.emf.teneo/tests/org.eclipse.emf.teneo.hibernate.test/"
				+ "src/org/eclipse/emf/teneo/hibernate/test/emf/demo/models/" + xsdFile;
		final URI modelUri = URI.createURI(xsdLocation);
		final Collection<EObject> ePackages = new XSDEcoreBuilder().generate(modelUri);
		System.err.println(ePackages);
		final EPackage ePackage = (EPackage) ePackages.toArray()[0];
		EPackage.Registry.INSTANCE.put(ePackage.getNsURI(), ePackage);
		return ePackage;
	}

	public DynamicDemoAction() {
		super(readEPackage("simple.xsd"));
	}

	@Override
	public Properties getExtraConfigurationProperties() {
		final Properties props = new Properties();
		props.put(PersistenceOptions.SET_PROXY, "true");
		props.put(PersistenceOptions.ENABLE_AUDITING, "false");
		props.put(PersistenceOptions.JOIN_TABLE_FOR_NON_CONTAINED_ASSOCIATIONS, "true");
		return props;
	}

	/** Creates an item, an address and links them to a po. */
	@Override
	public void doAction(TestStore store) {
		final EPackage ePackage = readEPackage("model.xsd");

		for (EClassifier eClassifier : ePackage.getEClassifiers()) {
			System.err.println("=====================================");
			System.err.println(eClassifier.getName());
			if (eClassifier instanceof EClass) {
				EClass eClass = (EClass) eClassifier;
				for (EStructuralFeature eFeature : eClass.getEStructuralFeatures()) {
					if (eFeature instanceof EReference) {
						EReference eReference = (EReference) eFeature;
						System.err.println("> " + eReference.getName() + " --> "
								+ eReference.getEType().getName() + " multi: " + eReference.getUpperBound());
					} else {
						EAttribute eAttribute = (EAttribute) eFeature;
						System.err
								.println("> " + eAttribute.getName() + " --> " + eAttribute.getEType().getName());
					}
				}
			} else if (eClassifier instanceof EEnum) {
				for (EEnumLiteral literal : ((EEnum) eClassifier).getELiterals()) {
					System.err.println(literal.getName());
				}
			}
		}

		HbEntityDataStore emfDataStore = new HbEntityDataStore();
		emfDataStore.setName(store.getDatabaseAdapter().getDbName());
		Properties props = new Properties();
		props.putAll(((HibernateTestStore) store)
				.getHibernateProperties((HibernateTestDBAdapter) store.getDatabaseAdapter()));
		props.put(PersistenceOptions.SET_PROXY, "true");
		props.put(PersistenceOptions.ENABLE_AUDITING, "false");
		props.put(PersistenceOptions.JOIN_TABLE_FOR_NON_CONTAINED_ASSOCIATIONS, "true");
		props.put(PersistenceOptions.ALSO_MAP_AS_CLASS, "true");
		props.put(PersistenceOptions.INHERITANCE_MAPPING, "JOINED");

		emfDataStore.setDataStoreProperties(props);
		emfDataStore.setEPackages(new EPackage[] { ePackage });
		emfDataStore.initialize();

		System.err.println(emfDataStore.getMappingXML());

		EFactory eFactory = ePackage.getEFactoryInstance();

		EClass employeeEClass = (EClass) ePackage.getEClassifier("Employee");
		EAttribute nameEAttribute = (EAttribute) employeeEClass.getEStructuralFeature("name");
		EAttribute employeeNumberEAttribute = (EAttribute) employeeEClass
				.getEStructuralFeature("employeeNumber");
		EEnum roleEnum = (EEnum) ePackage.getEClassifier("Role");
		EEnumLiteral managerRole = roleEnum.getEEnumLiteral(0);
		EAttribute roleEAttribute = (EAttribute) employeeEClass.getEStructuralFeature("role");

		EClass personEClass = (EClass) ePackage.getEClassifier("Person");
		EAttribute personNameEAttribute = (EAttribute) personEClass.getEStructuralFeature("name");

		EClass teamEClass = (EClass) ePackage.getEClassifier("Team");
		EAttribute teamNameEAttribute = (EAttribute) teamEClass.getEStructuralFeature("name");
		EReference captainEReference = (EReference) teamEClass.getEStructuralFeature("captain");
		EReference membersEReference = (EReference) teamEClass.getEStructuralFeature("members");

		{
			EntityManager em = emfDataStore.createEntityManager();
			EntityTransaction tx = em.getTransaction();
			tx.begin();
			// create an employee
			EObject employee = eFactory.create(employeeEClass);
			employee.eSet(nameEAttribute, "E1");
			employee.eSet(employeeNumberEAttribute, "1234");
			employee.eSet(roleEAttribute, managerRole);
			em.persist(employee);

			// create a person
			EObject person = eFactory.create(personEClass);
			person.eSet(personNameEAttribute, "P1");
			em.persist(person);

			tx.commit();
			em.close();
		}

		// query for the persons and create a team
		{
			EntityManager em = emfDataStore.createEntityManager();
			EntityTransaction tx = em.getTransaction();
			tx.begin();
			Query qry = em.createQuery("select p from Person p");
			List personList = qry.getResultList();
			// there should be two persons
			for (Object o : personList) {
				EObject person = (EObject) o;
				System.err.println(person.eClass().getName() + "-->" + person.eGet(personNameEAttribute));
			}

			// create a team
			EObject team = eFactory.create(teamEClass);
			team.eSet(teamNameEAttribute, "T1");
			team.eSet(captainEReference, personList.get(0));
			// add the persons
			for (Object o : personList) {
				EObject person = (EObject) o;
				((List<EObject>) team.eGet(membersEReference)).add(person);
			}
			em.persist(team);
			tx.commit();
			em.close();
		}

		// read the team back
		{
			EntityManager em = emfDataStore.createEntityManager();
			EntityTransaction tx = em.getTransaction();
			tx.begin();
			Query qry = em.createQuery("select t from Team t");
			EObject team = (EObject) qry.getSingleResult();
			System.err.println(team.eGet(captainEReference).getClass().getName());
			System.err.println(team.eGet(membersEReference).getClass().getName());

			// there should be two team members
			for (Object o : (List) team.eGet(membersEReference)) {
				EObject person = (EObject) o;
				System.err.println(person.eClass().getName() + "-->" + person.eGet(personNameEAttribute));
			}

			// the captain and the first team member are the exact same objects
			System.err
					.println(team.eGet(captainEReference) == ((List) team.eGet(membersEReference)).get(0));
			tx.commit();
			em.close();
		}

		// let's extend the model and create a new eclass
		{
			// the metamodel epackage and efactor
			final EcoreFactory ecoreFactory = EcoreFactory.eINSTANCE;
			final EcorePackage ecorePackage = EcorePackage.eINSTANCE;

			// create the eclass
			EClass clubEClass = ecoreFactory.createEClass();
			clubEClass.setName("Club");

			// and its attributes/references
			EAttribute clubName = ecoreFactory.createEAttribute();
			clubName.setName("name");
			clubName.setEType(ecorePackage.getEString());

			// annotate with some simple hibernate id
			final EAnnotation idAnnotation = EcoreFactory.eINSTANCE.createEAnnotation();
			idAnnotation.setSource("teneo.jpa");
			idAnnotation.getDetails().put("value", "@Id");
			clubName.getEAnnotations().add(idAnnotation);
			clubEClass.getEStructuralFeatures().add(clubName);

			EReference teams = ecoreFactory.createEReference();
			teams.setName("teams");
			teams.setEType(teamEClass);
			teams.setContainment(true);
			teams.setUpperBound(-1);
			clubEClass.getEStructuralFeatures().add(teams);

			ePackage.getEClassifiers().add(clubEClass);

			// recreate the database
			emfDataStore.initialize();
		}

		{
			// get the model
			EClass clubEClass = (EClass) ePackage.getEClassifier("Club");
			EAttribute clubNameEAttribute = clubEClass.getEAllAttributes().get(0);
			EReference teamsEReference = clubEClass.getEReferences().get(0);

			// create em
			EntityManager em = emfDataStore.createEntityManager();
			EntityTransaction tx = em.getTransaction();
			tx.begin();

			// query for the teams
			Query qry = em.createQuery("select t from Team t");
			EObject team = (EObject) qry.getSingleResult();

			// create and persist a club
			EObject club = eFactory.create(clubEClass);
			club.eSet(clubNameEAttribute, "C1");
			((List) club.eGet(teamsEReference)).add(team);
			em.persist(club);

			tx.commit();
			em.close();

		}

		emfDataStore.close();
	}
}