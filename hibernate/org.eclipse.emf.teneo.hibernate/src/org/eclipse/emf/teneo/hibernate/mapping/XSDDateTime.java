/**
 * <copyright>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *   Brian Vetter
 *   Martin Taal
 *   Alexandros Karypidis (bugzilla 207799)
 * </copyright>
 *
 * $Id: XSDDateTime.java,v 1.4 2010/11/12 09:33:33 mtaal Exp $
 */
package org.eclipse.emf.teneo.hibernate.mapping;

import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.eclipse.emf.teneo.hibernate.HbStoreException;
import org.eclipse.emf.teneo.util.EcoreDataTypes;
import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.usertype.UserType;

/**
 * Implements the hibernate UserType for EMF's XMLGregorianCalendar ("datetime" type in XSD).
 * 
 * @author <a href="mailto:bvetter@alterpoint.com">Brian Vetter</a>
 * @version $Revision
 */
public class XSDDateTime implements UserType {

	static final long serialVersionUID = 1;

	private static final int[] SQL_TYPES = new int[] { Types.TIMESTAMP };

	// local copy of the datatype facatory
	private final DatatypeFactory dataTypeFactory;

	public XSDDateTime() {
		try {
			dataTypeFactory = DatatypeFactory.newInstance();
		} catch (DatatypeConfigurationException e) {
			throw new HbStoreException("Exception ", e);
		}
	}

	/*
	 * Returns the DATETIME type that maps to the sql TIMESTAMP type
	 * 
	 * @see org.hibernate.type.NullableType#sqlType()
	 */
	public int[] sqlTypes() {
		return SQL_TYPES;
	}

	/*
	 * Just return the original value.
	 * 
	 * @see org.hibernate.type.MutableType#deepCopyNotNull(java.lang.Object)
	 */
	public Object deepCopy(Object value) {
		return value;
	}

	/*
	 * returns a name for the user type
	 * 
	 * @see org.hibernate.type.Type#getName()
	 */
	public String getName() {
		return "xmldatetime";
	}

	/*
	 * This returns an XMLGregorianCalendar.class type
	 * 
	 * @see org.hibernate.type.Type#getReturnedClass()
	 */
	@SuppressWarnings("rawtypes")
	public Class returnedClass() {
		return XMLGregorianCalendar.class;
	}

	/*
	 * @see org.hibernate.type.NullableType#isEqual(java.lang.Object, java.lang.Object)
	 */
	public boolean equals(Object x, Object y) throws HibernateException {
		if (x == y) {
			return true;
		}
		if (x == null || y == null) {
			return false;
		}
		if (x.getClass() != y.getClass()) {
			return false;
		}
		return x.equals(y);
	}

	/*
	 * Transform the date in the resultSet into a XMLGregorianCalendar instance.
	 * 
	 * @see org.hibernate.type.NullableType#get(java.sql.ResultSet, java.lang.String)
	 */
	public Object nullSafeGet(ResultSet rs, String[] names, SharedSessionContractImplementor session,
			Object owner) throws HibernateException, SQLException {
		// MT: changed this to timestamp to get the seconds right
		Timestamp ts = rs.getTimestamp(names[0]);
		if (ts == null) {
			return null;
		}
		return EcoreDataTypes.INSTANCE.getXMLGregorianCalendarDateTime(ts);
	}

	/*
	 * Transform the XMLGregorianCalendar into a timestamp type to store in the database
	 * 
	 * @see org.hibernate.type.NullableType#set(java.sql.PreparedStatement, java.lang.Object, int)
	 */
	public void nullSafeSet(PreparedStatement st, Object value, int index,
			SharedSessionContractImplementor session) throws HibernateException, SQLException {
		if (value == null) {
			st.setTimestamp(index, null);
			return;
		}
		Timestamp d = new Timestamp(((XMLGregorianCalendar) value).toGregorianCalendar().getTime()
				.getTime());
		st.setTimestamp(index, d);
	}


	/*
	 * @see org.hibernate.type.NullableType#toString(java.lang.Object)
	 */
	public String toString(Object val) {
		if (val == null) {
			return null;
		}
		return ((XMLGregorianCalendar) val).toString();
	}

	/*
	 * @see org.hibernate.type.NullableType#fromStringValue(java.lang.String)
	 */
	public Object fromStringValue(String s) throws HibernateException {
		return dataTypeFactory.newXMLGregorianCalendar(s);
	}

	public int hashCode(Object x) throws HibernateException {
		if (x == null) {
			return 0;
		}
		return x.toString().hashCode();
	}


	public boolean isMutable() {
		return false;
	}

	public Serializable disassemble(Object value) throws HibernateException {
		return toString(value);
	}

	public Object assemble(Serializable cached, Object owner) throws HibernateException {
		if (cached == null) {
			return null;
		}
		return fromStringValue(cached.toString());
	}

	public Object replace(Object original, Object target, Object owner) throws HibernateException {
		return original;
	}
}
