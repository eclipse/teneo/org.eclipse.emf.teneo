/**
 * <copyright>
 *
 * Copyright (c) 2005, 2006, 2007, 2008, 2021 Springsite BV (The Netherlands) and others
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *   Martin Taal
 * </copyright>
 *
 * $Id: HbEntityDataStore.java,v 1.37 2011/07/05 05:09:41 mtaal Exp $
 */

package org.eclipse.emf.teneo.hibernate;

import java.util.List;
import java.util.Map;

import javax.persistence.Cache;
import javax.persistence.EntityGraph;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.FlushModeType;
import javax.persistence.LockModeType;
import javax.persistence.PersistenceUnitUtil;
import javax.persistence.Query;
import javax.persistence.StoredProcedureQuery;
import javax.persistence.SynchronizationType;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.metamodel.Metamodel;

import org.hibernate.cfg.Configuration;
import org.hibernate.type.EntityType;

/**
 * Adds Hibernate Entitymanager behavior to the hbDataStore.
 * 
 * @author <a href="mailto:mtaal@elver.org">Martin Taal</a>
 * @version $Revision: 1.37 $
 */
public class HbEntityDataStore extends HbSessionDataStore implements EntityManagerFactory {

	private static final long serialVersionUID = 1L;

	/**
	 * Note: returns an instance of the {@link WrappedEntityManagerFactory} class.
	 */
	public EntityManagerFactory getEntityManagerFactory() {
		return new WrappedEntityManagerFactory((EntityManagerFactory) getSessionFactory());
	}

	/** Return a new session wrapper */
	@Override
	public SessionWrapper createSessionWrapper() {
		return new HbEntityManagerWrapper(this);
	}

	/**
	 * @return the hbConfiguration
	 */
	@Override
	public Configuration getHibernateConfiguration() {
		return getConfiguration();
	}

	/**
	 * Note: returns the {@link WrappedEntityManager} class.
	 */
	public EntityManager createEntityManager() {
		return getEntityManagerFactory().createEntityManager();
	}

	/**
	 * Note: returns the {@link WrappedEntityManager} class.
	 */
	@SuppressWarnings("rawtypes")
	public EntityManager createEntityManager(Map arg0) {
		return getEntityManagerFactory().createEntityManager(arg0);
	}

	public boolean isOpen() {
		return getEntityManagerFactory().isOpen();
	}

	public CriteriaBuilder getCriteriaBuilder() {
		return getEntityManagerFactory().getCriteriaBuilder();
	}

	public PersistenceUnitUtil getPersistenceUnitUtil() {
		return getEntityManagerFactory().getPersistenceUnitUtil();
	}

	public Map<String, Object> getProperties() {
		return getEntityManagerFactory().getProperties();
	}

	/**
	 * The HbEntityDataStore uses a wrapped entity manager factory and wrapped entity manager to work
	 * around an issue that the Hibernate entity manager/ query implementation does not resolve query
	 * parameters of the EntityType correctly. It determines a wrong type. See the statement about
	 * this in the javadoc of the Hibernate {@link EntityType#getReturnedClass()} method.
	 * 
	 * To get to the original Hibernate entity manager use the
	 * {@link WrappedEntityManagerFactory#getDelegate()} method.
	 * 
	 * @author mtaal
	 */
	public class WrappedEntityManagerFactory implements EntityManagerFactory {
		private EntityManagerFactory delegate;

		public WrappedEntityManagerFactory(EntityManagerFactory emf) {
			delegate = emf;
		}

		public EntityManagerFactory getDelegate() {
			return delegate;
		}

		public void setDelegate(EntityManagerFactory delegate) {
			this.delegate = delegate;
		}

		public void close() {
			delegate.close();
		}

		public EntityManager createEntityManager() {
			EntityManager em = delegate.createEntityManager();
			return new WrappedEntityManager(em);
		}

		public EntityManager createEntityManager(@SuppressWarnings("rawtypes") Map map) {
			EntityManager em = delegate.createEntityManager(map);
			return new WrappedEntityManager(em);
		}

		public Cache getCache() {
			return delegate.getCache();
		}

		public CriteriaBuilder getCriteriaBuilder() {
			return delegate.getCriteriaBuilder();
		}

		public Metamodel getMetamodel() {
			return delegate.getMetamodel();
		}

		public PersistenceUnitUtil getPersistenceUnitUtil() {
			return delegate.getPersistenceUnitUtil();
		}

		public Map<String, Object> getProperties() {
			return delegate.getProperties();
		}

		public boolean isOpen() {
			return delegate.isOpen();
		}

		public <T> void addNamedEntityGraph(String arg0, EntityGraph<T> arg1) {
			delegate.addNamedEntityGraph(arg0, arg1);
		}

		public void addNamedQuery(String arg0, Query arg1) {
			delegate.addNamedQuery(arg0, arg1);
		}

		public EntityManager createEntityManager(SynchronizationType arg0) {
			return delegate.createEntityManager(arg0);
		}

		public EntityManager createEntityManager(SynchronizationType arg0,
				@SuppressWarnings("rawtypes") Map arg1) {
			return delegate.createEntityManager(arg0, arg1);
		}

		public <T> T unwrap(Class<T> arg0) {
			return delegate.unwrap(arg0);
		}
	}

	/**
	 * See {@link WrappedEntityManagerFactory} for a description why this class is needed.
	 * 
	 * To get to the original Hibernate entity manager use the
	 * {@link WrappedEntityManager#getDelegateEntityManager()} method.
	 * 
	 * @author mtaal
	 * 
	 */
	public class WrappedEntityManager implements EntityManager {
		private EntityManager delegateEntityManager;

		public WrappedEntityManager(EntityManager em) {
			delegateEntityManager = em;
		}

		public void clear() {
			delegateEntityManager.clear();
		}

		public void close() {
			if (delegateEntityManager.isOpen()) {
				delegateEntityManager.close();
			}
		}

		public boolean contains(Object arg0) {
			return delegateEntityManager.contains(arg0);
		}

		public <T> TypedQuery<T> createNamedQuery(String arg0, Class<T> arg1) {
			return delegateEntityManager.createNamedQuery(arg0, arg1);
		}

		public Query createNamedQuery(String arg0) {
			return delegateEntityManager.createNamedQuery(arg0);
		}

		public Query createNativeQuery(String arg0, @SuppressWarnings("rawtypes") Class arg1) {
			return delegateEntityManager.createNativeQuery(arg0, arg1);
		}

		public Query createNativeQuery(String arg0, String arg1) {
			return delegateEntityManager.createNativeQuery(arg0, arg1);
		}

		public Query createNativeQuery(String arg0) {
			return delegateEntityManager.createNativeQuery(arg0);
		}

		public <T> TypedQuery<T> createQuery(CriteriaQuery<T> arg0) {
			return delegateEntityManager.createQuery(arg0);
		}

		public <T> TypedQuery<T> createQuery(String arg0, Class<T> arg1) {
			return delegateEntityManager.createQuery(arg0, arg1);
		}

		public Query createQuery(String arg0) {
			return delegateEntityManager.createQuery(arg0);
		}

		public void detach(Object arg0) {
			delegateEntityManager.detach(arg0);
		}

		public <T> T find(Class<T> arg0, Object arg1, LockModeType arg2, Map<String, Object> arg3) {
			return delegateEntityManager.find(arg0, arg1, arg2, arg3);
		}

		public <T> T find(Class<T> arg0, Object arg1, LockModeType arg2) {
			return delegateEntityManager.find(arg0, arg1, arg2);
		}

		public <T> T find(Class<T> arg0, Object arg1, Map<String, Object> arg2) {
			return delegateEntityManager.find(arg0, arg1, arg2);
		}

		public <T> T find(Class<T> arg0, Object arg1) {
			return delegateEntityManager.find(arg0, arg1);
		}

		public void flush() {
			delegateEntityManager.flush();
		}

		public CriteriaBuilder getCriteriaBuilder() {
			return delegateEntityManager.getCriteriaBuilder();
		}

		public Object getDelegate() {
			return delegateEntityManager.getDelegate();
		}

		public EntityManagerFactory getEntityManagerFactory() {
			return delegateEntityManager.getEntityManagerFactory();
		}

		public FlushModeType getFlushMode() {
			return delegateEntityManager.getFlushMode();
		}

		public LockModeType getLockMode(Object arg0) {
			return delegateEntityManager.getLockMode(arg0);
		}

		public Metamodel getMetamodel() {
			return delegateEntityManager.getMetamodel();
		}

		public Map<String, Object> getProperties() {
			return delegateEntityManager.getProperties();
		}

		public <T> T getReference(Class<T> arg0, Object arg1) {
			return delegateEntityManager.getReference(arg0, arg1);
		}

		public EntityTransaction getTransaction() {
			return delegateEntityManager.getTransaction();
		}

		public boolean isOpen() {
			return delegateEntityManager.isOpen();
		}

		public void joinTransaction() {
			delegateEntityManager.joinTransaction();
		}

		public void lock(Object arg0, LockModeType arg1, Map<String, Object> arg2) {
			delegateEntityManager.lock(arg0, arg1, arg2);
		}

		public void lock(Object arg0, LockModeType arg1) {
			delegateEntityManager.lock(arg0, arg1);
		}

		public <T> T merge(T arg0) {
			return delegateEntityManager.merge(arg0);
		}

		public void persist(Object arg0) {
			delegateEntityManager.persist(arg0);
		}

		public void refresh(Object arg0, LockModeType arg1, Map<String, Object> arg2) {
			delegateEntityManager.refresh(arg0, arg1, arg2);
		}

		public void refresh(Object arg0, LockModeType arg1) {
			delegateEntityManager.refresh(arg0, arg1);
		}

		public void refresh(Object arg0, Map<String, Object> arg1) {
			delegateEntityManager.refresh(arg0, arg1);
		}

		public void refresh(Object arg0) {
			delegateEntityManager.refresh(arg0);
		}

		public void remove(Object arg0) {
			delegateEntityManager.remove(arg0);
		}

		public void setFlushMode(FlushModeType arg0) {
			delegateEntityManager.setFlushMode(arg0);
		}

		public void setProperty(String arg0, Object arg1) {
			delegateEntityManager.setProperty(arg0, arg1);
		}

		public <T> T unwrap(Class<T> arg0) {
			return delegateEntityManager.unwrap(arg0);
		}

		public EntityManager getDelegateEntityManager() {
			return delegateEntityManager;
		}

		public void setDelegateEntityManager(EntityManager delegateEntityManager) {
			this.delegateEntityManager = delegateEntityManager;
		}

		public <T> EntityGraph<T> createEntityGraph(Class<T> arg0) {
			return delegateEntityManager.createEntityGraph(arg0);
		}

		public EntityGraph<?> createEntityGraph(String arg0) {
			return delegateEntityManager.createEntityGraph(arg0);
		}

		public StoredProcedureQuery createNamedStoredProcedureQuery(String arg0) {
			return delegateEntityManager.createNamedStoredProcedureQuery(arg0);
		}

		public Query createQuery(@SuppressWarnings("rawtypes") CriteriaUpdate arg0) {
			return delegateEntityManager.createQuery(arg0);
		}

		public Query createQuery(@SuppressWarnings("rawtypes") CriteriaDelete arg0) {
			return delegateEntityManager.createQuery(arg0);
		}

		public StoredProcedureQuery createStoredProcedureQuery(String arg0) {
			return delegateEntityManager.createStoredProcedureQuery(arg0);
		}

		public StoredProcedureQuery createStoredProcedureQuery(String arg0,
				@SuppressWarnings("rawtypes") Class... arg1) {
			return delegateEntityManager.createStoredProcedureQuery(arg0, arg1);
		}

		public StoredProcedureQuery createStoredProcedureQuery(String arg0, String... arg1) {
			return delegateEntityManager.createStoredProcedureQuery(arg0, arg1);
		}

		public EntityGraph<?> getEntityGraph(String arg0) {
			return delegateEntityManager.getEntityGraph(arg0);
		}

		public <T> List<EntityGraph<? super T>> getEntityGraphs(Class<T> arg0) {
			return delegateEntityManager.getEntityGraphs(arg0);
		}

		public boolean isJoinedToTransaction() {
			return delegateEntityManager.isJoinedToTransaction();
		}
	}

	// JPA 2.1
	public <T> void addNamedEntityGraph(String arg0, EntityGraph<T> arg1) {
		getEntityManagerFactory().addNamedEntityGraph(arg0, arg1);
	}

	public void addNamedQuery(String arg0, Query arg1) {
		getEntityManagerFactory().addNamedQuery(arg0, arg1);
	}

	public EntityManager createEntityManager(SynchronizationType arg0) {
		return getEntityManagerFactory().createEntityManager(arg0);
	}

	public EntityManager createEntityManager(SynchronizationType arg0,
			@SuppressWarnings("rawtypes") Map arg1) {
		return getEntityManagerFactory().createEntityManager(arg0, arg1);
	}

	public <T> T unwrap(Class<T> arg0) {
		return getEntityManagerFactory().unwrap(arg0);
	}

}