/**
 * <copyright>
 *
 * Copyright (c) 2005, 2006, 2007, 2008, 2021 Springsite BV (The Netherlands) and others
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *   Martin Taal
 * </copyright>
 *
 * $Id: HbBaseSessionDataStore.java,v 1.11 2010/11/11 10:28:18 mtaal Exp $
 */

package org.eclipse.emf.teneo.hibernate;

import java.sql.Connection;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.naming.NamingException;
import javax.naming.Reference;
import javax.persistence.EntityGraph;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceUnitUtil;
import javax.persistence.Query;
import javax.persistence.SynchronizationType;
import javax.persistence.criteria.CriteriaBuilder;

import org.hibernate.CustomEntityDirtinessStrategy;
import org.hibernate.EntityNameResolver;
import org.hibernate.HibernateException;
import org.hibernate.Interceptor;
import org.hibernate.MappingException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.SessionFactoryObserver;
import org.hibernate.StatelessSession;
import org.hibernate.StatelessSessionBuilder;
import org.hibernate.TypeHelper;
import org.hibernate.boot.spi.SessionFactoryOptions;
import org.hibernate.cache.spi.CacheImplementor;
import org.hibernate.cfg.Settings;
import org.hibernate.context.spi.CurrentTenantIdentifierResolver;
import org.hibernate.dialect.Dialect;
import org.hibernate.dialect.function.SQLFunctionRegistry;
import org.hibernate.engine.ResultSetMappingDefinition;
import org.hibernate.engine.jdbc.spi.JdbcServices;
import org.hibernate.engine.jdbc.spi.SqlExceptionHelper;
import org.hibernate.engine.profile.FetchProfile;
import org.hibernate.engine.query.spi.QueryPlanCache;
import org.hibernate.engine.spi.FilterDefinition;
import org.hibernate.engine.spi.NamedQueryDefinition;
import org.hibernate.engine.spi.NamedSQLQueryDefinition;
import org.hibernate.engine.spi.SessionBuilderImplementor;
import org.hibernate.engine.spi.SessionFactoryImplementor;
import org.hibernate.event.spi.EventEngine;
import org.hibernate.exception.spi.SQLExceptionConverter;
import org.hibernate.graph.spi.RootGraphImplementor;
import org.hibernate.id.IdentifierGenerator;
import org.hibernate.id.factory.IdentifierGeneratorFactory;
import org.hibernate.internal.FastSessionServices;
import org.hibernate.internal.SessionFactoryImpl;
import org.hibernate.metadata.ClassMetadata;
import org.hibernate.metadata.CollectionMetadata;
import org.hibernate.metamodel.spi.MetamodelImplementor;
import org.hibernate.persister.collection.CollectionPersister;
import org.hibernate.persister.entity.EntityPersister;
import org.hibernate.proxy.EntityNotFoundDelegate;
import org.hibernate.query.spi.NamedQueryRepository;
import org.hibernate.service.spi.ServiceRegistryImplementor;
import org.hibernate.stat.spi.StatisticsImplementor;
import org.hibernate.type.Type;
import org.hibernate.type.TypeResolver;

/**
 * Holds the sessionfactory related methods, makes the HbSessionDataStore better readable.
 * 
 * @author <a href="mailto:mtaal@elver.org">Martin Taal</a>
 * @version $Revision: 1.11 $
 */
@SuppressWarnings({ "deprecation", "rawtypes" })
public abstract class HbBaseSessionDataStore extends HbDataStore implements SessionFactory,
		SessionFactoryImplementor {

	private static final long serialVersionUID = 1L;

	/** The persistency manager factory */
	private SessionFactoryImplementor sessionFactory;

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.emf.teneo.hibernate.HbDataStore#close()
	 */
	@Override
	public void close() {
		if (isInitialized()) {
			closeSessionFactory();
			// this will call the close method again but because the
			// datastore
			// is not initialized anymore it won't get here
			HbHelper.INSTANCE.deRegisterDataStore(this);
		}
	}

	/**
	 * @return the sessionFactory
	 */
	@Override
	public SessionFactoryImplementor getSessionFactory() {
		if (!isInitialized()) {
			initialize();
		}
		return (SessionFactoryImplementor) sessionFactory;
	}

	// close session factory if set
	protected void closeSessionFactory() {
		if (sessionFactory != null && !sessionFactory.isClosed()) {
			sessionFactory.close();
			sessionFactory = null;
		}
		// do set initialized false after closing it
		setInitialized(false);
	}

	/**
	 * @param sessionFactory
	 *          the sessionFactory to set
	 */
	public void setSessionFactory(SessionFactoryImplementor sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public Session getCurrentSession() throws HibernateException {
		return getSessionFactory().getCurrentSession();
	}

	public Set getDefinedFilterNames() {
		return getSessionFactory().getDefinedFilterNames();
	}

	public FilterDefinition getFilterDefinition(String filterName) throws HibernateException {
		return getSessionFactory().getFilterDefinition(filterName);
	}

	public Reference getReference() throws NamingException {
		return getSessionFactory().getReference();
	}

	public boolean isClosed() {
		return getSessionFactory().isClosed();
	}

	public Session openSession() throws HibernateException {
		return getSessionFactory().openSession();
	}

	public Session openSession(Connection connection, Interceptor interceptor) {
		return ((SessionFactoryImpl) getSessionFactory()).withOptions().interceptor(interceptor)
				.connection(connection).openSession();
	}

	public Session openSession(Connection connection) {
		return ((SessionFactoryImpl) getSessionFactory()).withOptions().connection(connection)
				.openSession();
	}

	public Session openSession(Interceptor interceptor) throws HibernateException {
		return ((SessionFactoryImpl) getSessionFactory()).withOptions().interceptor(interceptor)
				.openSession();
	}

	public StatelessSession openStatelessSession() {
		return getSessionFactory().openStatelessSession();
	}

	public StatelessSession openStatelessSession(Connection connection) {
		return getSessionFactory().openStatelessSession(connection);
	}

	public SessionFactoryImplementor getSessionFactoryImplementor() {
		return (SessionFactoryImplementor) getSessionFactory();
	}

	public void addObserver(SessionFactoryObserver arg0) {
		getSessionFactoryImplementor().addObserver(arg0);
	}

	public boolean containsFetchProfileDefinition(String arg0) {
		return getSessionFactoryImplementor().containsFetchProfileDefinition(arg0);
	}

	public CollectionPersister getCollectionPersister(String arg0) throws MappingException {
		return getSessionFactoryImplementor().getCollectionPersister(arg0);
	}

	public Set<String> getCollectionRolesByEntityParticipant(String arg0) {
		return getSessionFactoryImplementor().getCollectionRolesByEntityParticipant(arg0);
	}

	public CurrentTenantIdentifierResolver getCurrentTenantIdentifierResolver() {
		return getSessionFactoryImplementor().getCurrentTenantIdentifierResolver();
	}

	public CustomEntityDirtinessStrategy getCustomEntityDirtinessStrategy() {
		return getSessionFactoryImplementor().getCustomEntityDirtinessStrategy();
	}

	public Dialect getDialect() {
		return getSessionFactoryImplementor().getDialect();
	}

	public EntityNotFoundDelegate getEntityNotFoundDelegate() {
		return getSessionFactoryImplementor().getEntityNotFoundDelegate();
	}

	public EntityPersister getEntityPersister(String arg0) throws MappingException {
		return getSessionFactoryImplementor().getEntityPersister(arg0);
	}

	public FetchProfile getFetchProfile(String arg0) {
		return getSessionFactoryImplementor().getFetchProfile(arg0);
	}

	public IdentifierGenerator getIdentifierGenerator(String arg0) {
		return getSessionFactoryImplementor().getIdentifierGenerator(arg0);
	}

	@Deprecated
	public IdentifierGeneratorFactory getIdentifierGeneratorFactory() {
		return getSessionFactoryImplementor().getIdentifierGeneratorFactory();
	}

	public String getIdentifierPropertyName(String arg0) throws MappingException {
		return getSessionFactoryImplementor().getIdentifierPropertyName(arg0);
	}

	public Type getIdentifierType(String arg0) throws MappingException {
		return getSessionFactoryImplementor().getIdentifierType(arg0);
	}

	public String getImportedClassName(String arg0) {
		return getSessionFactoryImplementor().getImportedClassName(arg0);
	}

	public JdbcServices getJdbcServices() {
		return getSessionFactoryImplementor().getJdbcServices();
	}

	public NamedQueryDefinition getNamedQuery(String arg0) {
		return getSessionFactoryImplementor().getNamedQuery(arg0);
	}

	public NamedSQLQueryDefinition getNamedSQLQuery(String arg0) {
		return getSessionFactoryImplementor().getNamedSQLQuery(arg0);
	}

	public QueryPlanCache getQueryPlanCache() {
		return getSessionFactoryImplementor().getQueryPlanCache();
	}

	public Type getReferencedPropertyType(String arg0, String arg1) throws MappingException {
		return getSessionFactoryImplementor().getReferencedPropertyType(arg0, arg1);
	}

	public ResultSetMappingDefinition getResultSetMapping(String arg0) {
		return getSessionFactoryImplementor().getResultSetMapping(arg0);
	}

	public SQLExceptionConverter getSQLExceptionConverter() {
		return getSessionFactoryImplementor().getSQLExceptionConverter();
	}

	public SqlExceptionHelper getSQLExceptionHelper() {
		return getSessionFactoryImplementor().getSQLExceptionHelper();
	}

	public ServiceRegistryImplementor getServiceRegistry() {
		return getSessionFactoryImplementor().getServiceRegistry();
	}

	public SessionFactoryOptions getSessionFactoryOptions() {
		return getSessionFactoryImplementor().getSessionFactoryOptions();
	}

	public Settings getSettings() {
		return getSessionFactoryImplementor().getSettings();
	}

	public SQLFunctionRegistry getSqlFunctionRegistry() {
		return getSessionFactoryImplementor().getSqlFunctionRegistry();
	}

	public StatisticsImplementor getStatisticsImplementor() {
		return getSessionFactoryImplementor().getStatisticsImplementor();
	}

	public TypeHelper getTypeHelper() {
		return getSessionFactoryImplementor().getTypeHelper();
	}

	public TypeResolver getTypeResolver() {
		return getSessionFactoryImplementor().getTypeResolver();
	}

	public Session openTemporarySession() throws HibernateException {
		return getSessionFactoryImplementor().openTemporarySession();
	}

	public SessionBuilderImplementor withOptions() {
		return getSessionFactoryImplementor().withOptions();
	}

	public StatelessSessionBuilder withStatelessOptions() {
		return getSessionFactoryImplementor().withStatelessOptions();
	}

	public Map<String, CollectionPersister> getCollectionPersisters() {
		return getSessionFactoryImplementor().getCollectionPersisters();
	}

	public Map<String, EntityPersister> getEntityPersisters() {
		return getSessionFactoryImplementor().getEntityPersisters();
	}

	public Iterable<EntityNameResolver> iterateEntityNameResolvers() {
		return getSessionFactoryImplementor().iterateEntityNameResolvers();
	}

	public void registerNamedQueryDefinition(String arg0, NamedQueryDefinition arg1) {
		getSessionFactoryImplementor().registerNamedQueryDefinition(arg0, arg1);
	}

	public void registerNamedSQLQueryDefinition(String arg0, NamedSQLQueryDefinition arg1) {
		getSessionFactoryImplementor().registerNamedSQLQueryDefinition(arg0, arg1);
	}

	public <T> void addNamedEntityGraph(String arg0, EntityGraph<T> arg1) {
		getSessionFactory().addNamedEntityGraph(arg0, arg1);
	}

	public void addNamedQuery(String arg0, Query arg1) {
		getSessionFactory().addNamedQuery(arg0, arg1);
	}

	public EntityManager createEntityManager() {
		return getSessionFactory().createEntityManager();
	}

	public EntityManager createEntityManager(Map arg0) {
		return getSessionFactory().createEntityManager(arg0);
	}

	public EntityManager createEntityManager(SynchronizationType arg0, Map arg1) {
		return getSessionFactory().createEntityManager(arg0, arg1);
	}

	public EntityManager createEntityManager(SynchronizationType arg0) {
		return getSessionFactory().createEntityManager(arg0);
	}

	public CacheImplementor getCache() {
		return getSessionFactory().getCache();
	}

	public ClassMetadata getClassMetadata(Class entityClass) {
		return getSessionFactory().getClassMetadata(entityClass);
	}

	public ClassMetadata getClassMetadata(String entityName) {
		return getSessionFactory().getClassMetadata(entityName);
	}

	public DeserializationResolver getDeserializationResolver() {
		return getSessionFactory().getDeserializationResolver();
	}

	public CollectionMetadata getCollectionMetadata(String roleName) {
		return getSessionFactory().getCollectionMetadata(roleName);
	}

	public Map<String, ClassMetadata> getAllClassMetadata() {
		return getSessionFactory().getAllClassMetadata();
	}

	public Map getAllCollectionMetadata() {
		return getSessionFactory().getAllCollectionMetadata();
	}

	public <T> List<EntityGraph<? super T>> findEntityGraphsByType(Class<T> entityClass) {
		return getSessionFactory().findEntityGraphsByType(entityClass);
	}

	public <T> List<RootGraphImplementor<? super T>> findEntityGraphsByJavaType(
			Class<T> entityClass) {
		return getSessionFactory().findEntityGraphsByJavaType(entityClass);
	}

	public RootGraphImplementor<?> findEntityGraphByName(String name) {
		return getSessionFactory().findEntityGraphByName(name);
	}

	public CriteriaBuilder getCriteriaBuilder() {
		return getSessionFactory().getCriteriaBuilder();
	}

	public Type resolveParameterBindType(Object bindValue) {
		return getSessionFactory().resolveParameterBindType(bindValue);
	}

	public Type resolveParameterBindType(Class clazz) {
		return getSessionFactory().resolveParameterBindType(clazz);
	}

	public String getUuid() {
		return getSessionFactory().getUuid();
	}

	public StatisticsImplementor getStatistics() {
		return getSessionFactory().getStatistics();
	}

	public EventEngine getEventEngine() {
		return getSessionFactory().getEventEngine();
	}

	public NamedQueryRepository getNamedQueryRepository() {
		return getSessionFactory().getNamedQueryRepository();
	}

	public FastSessionServices getFastSessionServices() {
		return getSessionFactory().getFastSessionServices();
	}

	public Type[] getReturnTypes(String queryString) {
		return getSessionFactory().getReturnTypes(queryString);
	}

	public MetamodelImplementor getMetamodel() {
		return getSessionFactory().getMetamodel();
	}

	public String[] getImplementors(String entityName) {
		return getSessionFactory().getImplementors(entityName);
	}

	public PersistenceUnitUtil getPersistenceUnitUtil() {
		return getSessionFactory().getPersistenceUnitUtil();
	}

	public Map<String, Object> getProperties() {
		return getSessionFactory().getProperties();
	}

	public String[] getReturnAliases(String queryString) {
		return getSessionFactory().getReturnAliases(queryString);
	}

	public boolean isOpen() {
		return getSessionFactory().isOpen();
	}

	public EntityPersister locateEntityPersister(Class byClass) {
		return getSessionFactory().locateEntityPersister(byClass);
	}

	public EntityPersister locateEntityPersister(String byName) {
		return getSessionFactory().locateEntityPersister(byName);
	}

	public <T> T unwrap(Class<T> arg0) {
		return getSessionFactory().unwrap(arg0);
	}
}