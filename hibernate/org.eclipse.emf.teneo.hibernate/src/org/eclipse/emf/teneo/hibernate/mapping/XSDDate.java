/**
 * <copyright>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *   Brian Vetter
 * </copyright>
 *
 * $Id: XSDDate.java,v 1.2 2007/07/04 19:27:28 mtaal Exp $
 */
package org.eclipse.emf.teneo.hibernate.mapping;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Date;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.eclipse.emf.teneo.hibernate.HbStoreException;
import org.eclipse.emf.teneo.util.EcoreDataTypes;
import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SharedSessionContractImplementor;

/**
 * Implements the hibernate UserType for EMF's XMLGregorianCalendar ("date" type in XSD).
 * 
 * @author <a href="mailto:bvetter@alterpoint.com">Brian Vetter</a>
 * @version $Id
 */
public class XSDDate extends XSDDateTime {

	static final long serialVersionUID = 1;

	private static final int[] SQL_TYPES = new int[] { Types.DATE };

	// local copy of the datatype facatory
	private final DatatypeFactory dataTypeFactory;

	public XSDDate() {
		try {
			dataTypeFactory = DatatypeFactory.newInstance();
		} catch (DatatypeConfigurationException e) {
			throw new HbStoreException("Exception ", e);
		}
	}

	/*
	 * Returns the DATETIME type that maps to the sql TIMESTAMP type
	 * 
	 * @see org.hibernate.type.NullableType#sqlType()
	 */
	public int[] sqlTypes() {
		return SQL_TYPES;
	}

	/*
	 * returns a name for the user type
	 * 
	 * @see org.hibernate.type.Type#getName()
	 */
	@Override
	public String getName() {
		return "xmldate";
	}

	/*
	 * Transform the date in the resultSet into a XMLGregorianCalendar instance.
	 * 
	 * @see org.hibernate.type.NullableType#get(java.sql.ResultSet, java.lang.String)
	 */
	@Override
	public Object nullSafeGet(ResultSet rs, String[] names, SharedSessionContractImplementor session,
			Object owner) throws HibernateException, SQLException {
		Date date = rs.getDate(names[0]);
		if (date == null) {
			return null;
		}
		return EcoreDataTypes.INSTANCE.getXMLGregorianCalendar(date);
	}

	/*
	 * Transform the XMLGregorianCalendar into a timestamp type to store in the database
	 * 
	 * @see org.hibernate.type.NullableType#set(java.sql.PreparedStatement, java.lang.Object, int)
	 */
	public void nullSafeSet(PreparedStatement st, Object value, int index,
			SharedSessionContractImplementor session) throws HibernateException, SQLException {
		if (value == null) {
			st.setDate(index, null);
			return;
		}
		java.sql.Date d = new java.sql.Date(((XMLGregorianCalendar) value).toGregorianCalendar()
				.getTime().getTime());
		st.setDate(index, d);
	}

	/*
	 * @see org.hibernate.type.NullableType#fromStringValue(java.lang.String)
	 */
	@Override
	public Object fromStringValue(String s) throws HibernateException {
		return dataTypeFactory.newXMLGregorianCalendar(s);
	}
}
